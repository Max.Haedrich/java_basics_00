/**
 * Copyright (c) 2020-2024 Sven Gothel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package sic.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * Siehe Logik, JUC2 02.01 Logic01
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test0201Logik01 {

    /**
     * Returns the result of: A ∧ B
     */
    boolean und(final boolean a, final boolean b) {
        return a && b;
    }

    /**
     * Returns the result of: A ⊕ B := A ∧ ¬B ∨ ¬A ∧ B
     *
     * logisch-nicht operator ¬ -> !
     */
    boolean xor(final boolean a, final boolean b) {
        return a && !b || !a && b;
    }

    @Test
    void test00() {
        // bool'sche variable, logik, wahr (true, 1) oder falsch (false, 0)
        final boolean T = true;  // immutable (konstante) booleans
        final boolean F = false; // ..

        // logisch-und operator ∧ -> '&&'
        Assertions.assertEquals(false, F && F);
        Assertions.assertEquals(false, F && T);
        Assertions.assertEquals(false, T && F);
        Assertions.assertEquals(true,  T && T);
        // logisch-und operator ∧ -> '&&' .. mittels funktion und(a, b)
        Assertions.assertEquals(false, und(F, F));
        Assertions.assertEquals(false, und(F, T));
        Assertions.assertEquals(false, und(T, F));
        Assertions.assertEquals(true,  und(T, T));

        // logisch-oder operator ∨ -> '||'
        Assertions.assertEquals(false, F || F);
        Assertions.assertEquals(true,  F || T);
        Assertions.assertEquals(true,  T || F);
        Assertions.assertEquals(true,  T || T);

        // logisch-exclusive-oder (xor) ⊕
        Assertions.assertEquals(false, xor(F, F));
        Assertions.assertEquals(true,  xor(F, T));
        Assertions.assertEquals(true,  xor(T, F));
        Assertions.assertEquals(false, xor(T, T));
    }

    @Test
    void test01() {
    }

    @Test
    void test02() {
    }
}
